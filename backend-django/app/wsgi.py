# https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
from __future__ import unicode_literals

import os

from django.core.wsgi import get_wsgi_application
from whitenoise import WhiteNoise

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "waecm.settings")
application = get_wsgi_application()

# wrap WSGI application in WhiteNoise
application = WhiteNoise(application)
