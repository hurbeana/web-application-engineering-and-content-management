from drf_yasg import openapi as yasg_openapi
from drf_yasg.views import get_schema_view
from rest_framework.permissions import AllowAny

yasg_schema_view = get_schema_view(
    yasg_openapi.Info(
        title="Backend API",
        default_version="1.x",
        description="Backend API documentation",
    ),
    public=True,  # True = Include endpoints the current user has no access to
    permission_classes=(AllowAny,),
)
