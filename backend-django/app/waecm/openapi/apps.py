from django.apps import AppConfig


class OpenAPIConfig(AppConfig):
    name = "waecm.openapi"
