from django.urls import path
from rest_framework.schemas import get_schema_view
from waecm.core.routers import get_api_router
from waecm.openapi.views import yasg_schema_view

router = get_api_router()

urlpatterns = [
    path(
        "openapi/schema/",
        get_schema_view(title="API Documentation"),
        name="openapi-schema",
    ),
    path(
        "openapi/",
        yasg_schema_view.with_ui("swagger", cache_timeout=0),
        name="swagger-ui",
    ),
]
