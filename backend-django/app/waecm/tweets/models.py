import logging

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db.models import SET_NULL, DateTimeField, Model, OneToOneField, TextField
from django.utils import timezone
from twitter import TwitterError
from waecm.core.models import BaseModel
from waecm.feeds.models import News
from waecm.tweets.utils import get_twitter_api, truncate

LOGGER = logging.getLogger(__name__)


def get_default_created_at():
    return timezone.now()


class Tweet(BaseModel):
    news = OneToOneField(
        to=News,
        null=True,
        blank=True,
        on_delete=SET_NULL,
        related_name="tweet",
    )

    # TextField to be independent from changes by Twitter
    twitter_id = TextField(
        default="",
        blank=True,
    )

    # stores the message as posted to Twitter
    message = TextField(
        # index the message, so we can easily check that we don't tweet the same thing twice
        db_index=True,
    )

    created_at = DateTimeField(
        default=get_default_created_at,
    )

    class Meta:
        ordering = ("-created_at",)

    def __str__(self):
        return f"#{self.pk}: {self.message}"

    @staticmethod
    def for_news(news: News):
        url_separator = ": "
        max_text_length = settings.TWITTER_MAX_TWEET_LENGTH - len(url_separator) - settings.TWITTER_URL_LENGTH
        text = truncate(news.message, max_text_length)

        if len(text) > 0:
            message = text + url_separator + news.url
        else:
            message = news.url

        if Tweet.objects.filter(message=message).exists():
            raise ValidationError(f"Tweet message has been posted already: {message}")

        return Tweet(news=news, message=message)

    def post_to_twitter(self):
        """ Posts the tweet to Twitter and saves the tweet URL. Raises an exception in case of an error. """

        # https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-update
        try:
            twitter = get_twitter_api()
            response = twitter.statuses.update(status=self.message)
            self.message = response["text"]
            self.twitter_id = response["id_str"]
            self.save()
            LOGGER.info("Tweeted: " + self.message)
        except TwitterError:
            LOGGER.exception(f"Failed to post to Twitter. Content: <{self.message}>")
            raise
