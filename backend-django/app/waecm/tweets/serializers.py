from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import ModelSerializer
from waecm.feeds.serializers import NewsSerializer
from waecm.tweets.models import Tweet


class TweetSerializer(ModelSerializer):
    message = ReadOnlyField(read_only=True)
    news = NewsSerializer(read_only=True)

    class Meta:
        model = Tweet
        fields = "__all__"
