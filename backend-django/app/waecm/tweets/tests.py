from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from waecm.feeds.models import Feed

User = get_user_model()


class TweetsTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="user1", password="secret")
        self.client.force_login(self.user)

    def test_tweets_list_returns_204_if_there_are_no_feeds(self):
        Feed.objects.all().delete()
        response = self.client.get(reverse("tweets-list"))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, response.content.decode())
