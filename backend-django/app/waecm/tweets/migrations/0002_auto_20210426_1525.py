# Generated by Django 2.2.20 on 2021-04-26 15:25

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tweets", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="tweet",
            name="message",
            field=models.TextField(db_index=True),
        ),
        migrations.AlterField(
            model_name="tweet",
            name="news",
            field=models.OneToOneField(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="tweet",
                to="feeds.News",
            ),
        ),
    ]
