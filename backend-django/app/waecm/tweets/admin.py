from django.contrib import admin
from django.contrib.admin import ModelAdmin
from waecm.tweets.models import Tweet


@admin.register(Tweet)
class TweetAdmin(ModelAdmin):
    list_display = (
        "__str__",
        "created_at",
    )
    list_filter = ("created_at",)
    search_fields = ("message",)
    autocomplete_fields = ("news",)
