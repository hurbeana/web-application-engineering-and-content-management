# https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/
from django.core.management import BaseCommand
from waecm.feeds.models import News
from waecm.tweets.models import Tweet


def tweet_news():
    untweeted_news_of_active_feeds = News.objects.filter(
        tweet__isnull=True,
        feed__isnull=False,
        feed__active=True,
    )
    news = untweeted_news_of_active_feeds.first()

    if news:
        tweet = Tweet.for_news(news)
        tweet.post_to_twitter()
        tweet.save()


class Command(BaseCommand):
    help = "Tweets a news message that has not yet been tweeted."

    def handle(self, *args, **options):
        tweet_news()
