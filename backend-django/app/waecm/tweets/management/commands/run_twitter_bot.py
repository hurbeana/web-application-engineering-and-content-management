# https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/
import logging
from time import sleep

from django.conf import settings
from django.core.management import BaseCommand
from waecm.feeds.management.commands.read_feeds import read_feeds
from waecm.tweets.management.commands.tweet_news import tweet_news

LOGGER = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "Periodically reads feeds and posts tweets."

    def handle(self, *args, **options):
        try:
            while True:
                LOGGER.info("Twitter Bot: Reading feeds and posting new messages to Twitter")
                read_feeds()
                tweet_news()
                sleep(settings.BOT_INTERVAL_SECONDS)
        except KeyboardInterrupt:
            pass
