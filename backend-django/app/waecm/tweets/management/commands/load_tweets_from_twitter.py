# https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/
import logging
from datetime import datetime

import pytz
from django.core.management import BaseCommand
from waecm.tweets.models import Tweet
from waecm.tweets.utils import get_twitter_api

LOGGER = logging.getLogger(__name__)


def load_tweets_from_twitter():
    # https://developer.twitter.com/en/docs/twitter-api/v1/tweets/timelines/api-reference/get-statuses-home_timeline
    twitter = get_twitter_api()
    response = twitter.statuses.home_timeline()
    LOGGER.info(f"Loaded <{len(response)}> tweets from Twitter")

    new_tweet_count = 0
    for twitter_tweet in response:
        created_at_str = twitter_tweet["created_at"]
        created_at_parsed = datetime.strptime(created_at_str, "%a %b %d %H:%M:%S +0000 %Y").replace(tzinfo=pytz.UTC)
        tweet, created = Tweet.objects.get_or_create(
            twitter_id=twitter_tweet["id_str"],
            defaults={
                "message": twitter_tweet["text"],
                "created_at": created_at_parsed,
            },
        )
        if created:
            new_tweet_count += 1

    LOGGER.info(f"Saved <{new_tweet_count}> tweets to database")


class Command(BaseCommand):
    help = "Loads the recent tweets from Twitter into our database."

    def handle(self, *args, **options):
        load_tweets_from_twitter()
