from twitter import OAuth, Twitter
from waecm import settings


def truncate(message: str, max_length: int):
    ellipsis_symbol = settings.TWITTER_ELLIPSIS_SYMBOL

    if len(message) > max_length:
        end_index = max_length - len(ellipsis_symbol)
        return message[:end_index] + ellipsis_symbol
    else:
        return message


def get_twitter_api():
    auth = OAuth(
        token=settings.TWITTER_ACCESS_TOKEN,
        token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET,
        consumer_key=settings.TWITTER_API_KEY,
        consumer_secret=settings.TWITTER_API_SECRET_KEY,
    )
    return Twitter(auth=auth)
