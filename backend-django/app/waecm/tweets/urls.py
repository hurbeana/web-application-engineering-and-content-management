from waecm.core.routers import get_api_router
from waecm.tweets.viewsets import TweetViewSet

router = get_api_router()
router.register(r"tweets", TweetViewSet, basename="tweets")

urlpatterns = []
