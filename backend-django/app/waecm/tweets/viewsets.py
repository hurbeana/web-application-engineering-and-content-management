from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from waecm.feeds.models import Feed
from waecm.tweets.models import Tweet
from waecm.tweets.serializers import TweetSerializer


class TweetViewSet(ReadOnlyModelViewSet):
    queryset = Tweet.objects.all()
    serializer_class = TweetSerializer

    def list(self, request, *args, **kwargs):
        # return HTTP 204 in case there are no feeds
        if Feed.objects.all().count() <= 0:
            return Response(
                data=None,
                status=status.HTTP_204_NO_CONTENT,
            )

        return super().list(request, *args, **kwargs)
