from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from waecm.core.routers import get_api_router

router = get_api_router()

urlpatterns = [
    path("", include("waecm.core.urls")),
    path("", include("waecm.openapi.urls")),
    path("", include("waecm.feeds.urls")),
    path("", include("waecm.tweets.urls")),
    path("api/", include(router.urls)),
    path("admin/", admin.site.urls),
    # redirect to admin as fallback
    path("", RedirectView.as_view(url="admin/")),
]

# serve static and media files
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
