from waecm.core.routers import get_api_router
from waecm.feeds.viewsets import FeedViewSet

router = get_api_router()
router.register(r"feeds", FeedViewSet, basename="feeds")

urlpatterns = []
