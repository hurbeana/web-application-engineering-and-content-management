from django.contrib import admin
from django.contrib.admin import ModelAdmin
from waecm.feeds.models import Feed, News


@admin.register(Feed)
class FeedAdmin(ModelAdmin):
    list_display = (
        "__str__",
        "created_by",
        "created_at",
        "last_modified_by",
        "last_modified_at",
    )
    list_filter = (
        "created_at",
        "created_by",
        "last_modified_at",
        "last_modified_by",
    )
    search_fields = ("url",)
    autocomplete_fields = (
        "created_by",
        "last_modified_by",
    )


@admin.register(News)
class NewsAdmin(ModelAdmin):
    list_display = (
        "__str__",
        "url",
        "feed",
        "created_at",
    )
    list_filter = (
        "feed",
        "created_at",
    )
    search_fields = (
        "message",
        "url",
    )
    autocomplete_fields = ("feed",)
