import logging
import os

import feedparser
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.db.models import (
    SET_NULL,
    BooleanField,
    CharField,
    DateTimeField,
    FileField,
    ForeignKey,
    Model,
    TextField,
    URLField,
)
from django.utils.translation import ugettext as _
from django_userforeignkey.models.fields import UserForeignKey
from django_userforeignkey.request import get_current_user
from waecm.core.models import BaseModel

LOGGER = logging.getLogger(__name__)


def get_default_feed_user():
    user = get_current_user()
    return user if not user.is_anonymous else None


class Feed(BaseModel):
    """ Represents an RSS feed. """

    # URL shouldn't have more than 2_000 characters
    # https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers
    url = URLField(
        max_length=2_000,
    )

    keywords = ArrayField(
        base_field=CharField(
            max_length=1_000,
            blank=True,
            default="",
        ),
    )

    all_keywords_required = BooleanField()

    created_by = UserForeignKey(
        null=True,
        blank=True,
        default=get_default_feed_user,
        on_delete=SET_NULL,
        related_name="feeds_created",
    )

    last_modified_by = UserForeignKey(
        null=True,
        blank=True,
        default=get_default_feed_user,
        on_delete=SET_NULL,
        related_name="feeds_last_modified",
    )

    created_at = DateTimeField(auto_now_add=True)
    last_modified_at = DateTimeField(auto_now=True)

    icon = FileField(upload_to="images/", null=True, blank=True)
    active = BooleanField(default=True)

    def __str__(self):
        return f"#{self.pk}: {self.url}"

    def clean(self):
        self.last_modified_by = get_default_feed_user()
        self.clean_icon()
        self.clean_keywords()

        # make sure that there are no more than allowed feeds per user
        if self.created_by.feeds_created.count() >= settings.MAX_FEEDS_PER_USER:
            raise ValidationError(
                _("You can't create more feeds. {} are allowed per user.").format(settings.MAX_FEEDS_PER_USER)
            )

    def clean_icon(self):
        if not self.icon:
            return

        # assert maximum file size
        if self.icon.size > settings.MAX_FEED_ICON_FILE_SIZE_IN_KB * 1_024:
            raise ValidationError(
                {
                    "icon": _("The icon file is too big. Max allowed: {} KB.").format(
                        settings.MAX_FEED_ICON_FILE_SIZE_IN_KB
                    )
                }
            )

        # assert allowed file types
        filename, extension = os.path.splitext(self.icon.path)
        extension = extension.lower()
        if extension.startswith("."):
            extension = extension[1:]

        if extension not in settings.ALLOWED_FEED_ICON_EXTENSIONS:
            raise ValidationError(
                {
                    "icon": _("Invalid icon file extension: <{extension}>. Allowed extensions: {allowed}").format(
                        extension=extension,
                        allowed=settings.ALLOWED_FEED_ICON_EXTENSIONS,
                    )
                }
            )

    def clean_keywords(self):
        # assert that there is at least one keyword
        if not self.keywords or len(self.keywords) < 1:
            raise ValidationError({"keywords": _("At least one keyword is required.")})

        # assert maximum amount of keywords
        if len(self.keywords) > settings.MAX_KEYWORDS_PER_FEED:
            raise ValidationError(
                {"keywords": _("Too many keywords. {} are allowed.").format(settings.MAX_KEYWORDS_PER_FEED)}
            )

        # assert minimum length of keywords
        for keyword in self.keywords:
            if len(keyword) < settings.MIN_KEYWORD_LENGTH:
                raise ValidationError(
                    {
                        "keywords": _(
                            "Keyword <{keyword}> is too short. Use at least {min_chars} characters per keyword."
                        ).format(keyword=keyword, min_chars=settings.MIN_KEYWORD_LENGTH)
                    }
                )

        # make sure keywords are split (necessary for multipart requests)
        LOGGER.debug(f"keywords before processing: {self.keywords}")
        split_keywords = []
        for keyword in self.keywords:
            split_keywords += keyword.split(",")

        self.keywords = [kw.strip() for kw in split_keywords if kw]
        LOGGER.debug(f"keywords after processing: {self.keywords}")

    def load_news(self):
        """ Loads all matching entries from the feed. """

        # https://feedparser.readthedocs.io/en/latest/introduction.html
        parsed = feedparser.parse(self.url)

        # make sure that entries contain the required keywords
        matching_entries = [entry for entry in parsed.entries if self.rss_entry_matches_keywords(entry)]

        created_news = []
        for entry in matching_entries:
            # make sure that entries are really new/unique (do not exist in our database yet)
            news, created = News.objects.get_or_create(
                # check if there is an element with the following field values:
                message=entry["title"],
                url=entry["link"],
                # add other information in case a new instance is created:
                defaults={
                    "feed": self,
                },
            )
            if created:
                created_news.append(news)

        LOGGER.info(
            "Found <{news}> news in feed <{feed}>. {matching} matching, {created} created.".format(
                news=len(parsed.entries),
                feed=self,
                matching=len(matching_entries),
                created=len(created_news),
            )
        )

    def rss_entry_matches_keywords(self, entry):
        full_text = entry.get("title", "") + " " + entry.get("description", "")
        return self.text_matches_keywords(full_text)

    def text_matches_keywords(self, text):
        if self.all_keywords_required:
            return self.text_contains_all_keywords(text)
        else:
            return self.text_contains_any_keyword(text)

    def text_contains_all_keywords(self, text):
        lowercase_text = text.lower()

        for keyword in self.keywords:
            if keyword.lower() not in lowercase_text:
                return False

        return True

    def text_contains_any_keyword(self, text):
        lowercase_text = text.lower()

        for keyword in self.keywords:
            if keyword.lower() in lowercase_text:
                return True

        return False


class News(BaseModel):
    """ Represents a news message. """

    class Meta:
        verbose_name_plural = "News"

    feed = ForeignKey(
        to=Feed,
        null=True,
        blank=True,
        on_delete=SET_NULL,
    )

    url = URLField(
        max_length=2_000,
        default="",
    )

    message = TextField()

    created_at = DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"#{self.pk}: {self.message[:50]}"
