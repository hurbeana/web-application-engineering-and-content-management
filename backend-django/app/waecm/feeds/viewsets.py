from rest_framework.viewsets import ModelViewSet
from waecm.feeds.models import Feed
from waecm.feeds.serializers import FeedSerializer


class FeedViewSet(ModelViewSet):
    queryset = Feed.objects.all()
    serializer_class = FeedSerializer
