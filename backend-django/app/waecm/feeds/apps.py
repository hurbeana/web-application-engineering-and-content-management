from django.apps import AppConfig


class FeedsConfig(AppConfig):
    name = "waecm.feeds"
