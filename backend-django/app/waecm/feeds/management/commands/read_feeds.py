# https://docs.djangoproject.com/en/2.2/howto/custom-management-commands/
import logging

from django.core.management import BaseCommand
from waecm.feeds.models import Feed

LOGGER = logging.getLogger(__name__)


def read_feeds():
    active_feeds = Feed.objects.filter(active=True)
    for feed in active_feeds:
        try:
            feed.load_news()
        except Exception:
            LOGGER.exception(f"Failed to load news for feed <{feed}>")


class Command(BaseCommand):
    help = "Loads news from all feeds."

    def handle(self, *args, **options):
        read_feeds()
