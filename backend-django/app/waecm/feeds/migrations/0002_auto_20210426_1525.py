# Generated by Django 2.2.20 on 2021-04-26 15:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("feeds", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="news",
            name="url",
            field=models.URLField(default="", max_length=2000),
        ),
        migrations.AlterField(
            model_name="feed",
            name="url",
            field=models.URLField(max_length=2000, unique=True),
        ),
    ]
