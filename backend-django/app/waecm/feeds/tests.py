from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

User = get_user_model()

ORF_NEWS_FEED_URL = "https://rss.orf.at/news.xml"


class FeedsTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="user1", password="secret")
        self.client.force_login(self.user)

    def test_url_is_validated(self):
        response = self.client.post(
            reverse("feeds-list"),
            data={
                "url": "hello@iam.invalid",
                "keywords": [
                    "covid-19",
                ],
                "all_keywords_required": False,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_min_keyword_length_is_validated(self):
        response = self.client.post(
            reverse("feeds-list"),
            data={
                "url": ORF_NEWS_FEED_URL,
                "keywords": [
                    "ab",
                ],
                "all_keywords_required": False,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_max_keyword_count_is_validated(self):
        response = self.client.post(
            reverse("feeds-list"),
            data={
                "url": ORF_NEWS_FEED_URL,
                "keywords": [
                    "covid-19",
                    "corona",
                    "sars",
                    "impfung",
                ],
                "all_keywords_required": False,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_at_least_one_keyword_is_required(self):
        response = self.client.post(
            reverse("feeds-list"),
            data={
                "url": ORF_NEWS_FEED_URL,
                "keywords": [],
                "all_keywords_required": False,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_max_feeds_per_user(self):
        for i in range(1, 4):
            response = self.client.post(
                reverse("feeds-list"),
                data={
                    "url": f"https://rss.orf.at/news{i}.xml",
                    "keywords": [
                        "covid-19",
                    ],
                    "all_keywords_required": False,
                },
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content.decode())

        response = self.client.post(
            reverse("feeds-list"),
            data={
                "url": f"https://rss.orf.at/news4.xml",
                "keywords": [
                    "covid-19",
                ],
                "all_keywords_required": False,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
