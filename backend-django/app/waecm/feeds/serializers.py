from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import ModelSerializer
from waecm.core.serializers import UserSerializer
from waecm.feeds.models import Feed, News


class FeedSerializer(ModelSerializer):
    created_at = ReadOnlyField()
    last_modified_at = ReadOnlyField()
    created_by = UserSerializer(read_only=True)
    last_modified_by = UserSerializer(read_only=True)

    class Meta:
        model = Feed
        fields = "__all__"


class NewsSerializer(ModelSerializer):
    feed = FeedSerializer()

    class Meta:
        model = News
        fields = "__all__"
