from django.core.exceptions import ValidationError
from rest_framework import status, views
from rest_framework.response import Response


def get_error_message(exc: ValidationError):
    if hasattr(exc, "messages"):
        return " ".join(exc.messages)
    elif hasattr(exc, "message"):
        return exc.message
    else:
        return str(exc)


def error_handler(exc: Exception, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = views.exception_handler(exc, context)

    # Now add the status fields to the response.
    if response is not None:
        response.data["success"] = False
        response.data["status_code"] = getattr(exc, "status_code", None) or response.status_code
    elif isinstance(exc, ValidationError):
        response = Response(
            status=status.HTTP_400_BAD_REQUEST,
            data={
                "success": False,
                "error_message": get_error_message(exc),
            },
        )

    return response
