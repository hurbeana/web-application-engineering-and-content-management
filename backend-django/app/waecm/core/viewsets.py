from django.http import JsonResponse
from rest_framework.fields import BooleanField
from rest_framework.mixins import ListModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import Serializer
from rest_framework.viewsets import GenericViewSet


class LoginSerializer(Serializer):
    success = BooleanField(read_only=True)


class LoginViewSet(ListModelMixin, GenericViewSet):
    """
    Provides an explicit endpoint for authentication.

    We could use an APIView here, but that would hide the endpoint from the browsable API,
    therefore we just go with a standard ViewSet that only has a list endpoint.

    The actual authentication against the OIDC provider happens in the drf-oidc-auth library, which provides an
    authentication backend for the Django framework.
    The most relevant class is `JSONWebTokenAuthentication`. It reads the token from the HTTP header,
    fetches the public keys from the OIDC provider and validates the token against the key set.
    Another library (`authlib`) is used internally to do the actual validation (via the `JsonWebToken` class).

    Library sources:
    * https://github.com/ByteInternet/drf-oidc-auth
    * https://github.com/lepture/authlib
    """

    # explicitly enforce authentication, so the endpoint still works as expected,
    # even if the default permission_classes change
    permission_classes = [IsAuthenticated]
    serializer_class = LoginSerializer

    def list(self, request, *args, **kwargs):
        # the actual authentication is done by the Authentication class of drf-oidc-auth
        # so if we get here, we can simply return an "ok" message
        return JsonResponse(data={"success": True})
