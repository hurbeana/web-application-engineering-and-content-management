from waecm.core.routers import get_api_router
from waecm.core.viewsets import LoginViewSet

router = get_api_router()
router.register(r"login", LoginViewSet, basename="login")

urlpatterns = []
