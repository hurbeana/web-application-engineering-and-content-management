from rest_framework import routers

__router = None


def get_api_router():
    global __router

    if not __router:
        __router = routers.DefaultRouter()

    return __router
