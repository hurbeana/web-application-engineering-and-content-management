import logging

from django.contrib.auth import get_user_model

LOGGER = logging.getLogger(__name__)

User = get_user_model()


def resolve_user_from_oidc(request, id_token):
    """ This function is called by drf-oidc-auth to find a local user entry for a verified id_token. """

    username = id_token.get("preferred_username", None)
    LOGGER.debug(f"Resolving OIDC user <{username}>")

    # todo: match user based on "sub" claim of the token

    user = User.objects.filter(username=username).first()
    if user:
        LOGGER.info(f"Resolved user: <{username}> is user with ID {user.pk}")
    else:
        user = User.objects.create_user(username)
        LOGGER.info(f"Created new user: <{username}> now has ID {user.pk}")

    return user
