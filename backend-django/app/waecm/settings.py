# https://docs.djangoproject.com/en/2.2/topics/settings/
import os
import time

import environ
from django.db import connections
from django.db.utils import OperationalError
from django.urls import reverse_lazy

project_root = environ.Path(__file__) - 2  # two folders back

env_file_name = os.environ.get("ENV_FILE", ".env")
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
environ.Env.read_env(env_file=project_root(env_file_name))

DEBUG = env("DEBUG", default=False)
TEMPLATE_DEBUG = env("TEMPLATE_DEBUG", default=DEBUG)
SECRET_KEY = env("SECRET_KEY")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=[])

DATABASES = {
    "default": env.db(),
}

while True:
    conn = connections["default"]  # or some other key in `DATBASES`
    try:
        c = conn.cursor()
        break
    except OperationalError:
        print("Postgres Not Ready...")
        time.sleep(0.5)


INSTALLED_APPS = [
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # REST API
    "corsheaders",
    "rest_framework",
    # 3rd party
    "django_userforeignkey",
    "drf_yasg",
    # App
    "waecm.core",
    "waecm.openapi",
    "waecm.feeds",
    "waecm.tweets",
]

MIDDLEWARE = [
    # CORS
    "corsheaders.middleware.CorsMiddleware",
    # Django
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",  # needs to be above all others, except Security
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    # 3rd party
    "django_userforeignkey.middleware.UserForeignKeyMiddleware",
]

ROOT_URLCONF = "waecm.urls"

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

STATIC_ROOT = project_root("static")
MEDIA_ROOT = project_root("media")
WHITENOISE_ROOT = project_root("root_static")

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "wsgi.application"

USE_I18N = False
USE_L10N = False
USE_TZ = False

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "standard": {"format": "%(asctime)s [%(levelname)s] [%(name)s] %(message)s"},
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "standard",
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": project_root("logs") + "/application.log",
            "formatter": "standard",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
            "propagate": True,
            "formatter": "verbose",
        },
        "django.request": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
            "propagate": False,
            "formatter": "verbose",
        },
        "rest_framework": {
            "handlers": ["console", "file"],
            "level": "DEBUG",
            "propagate": True,
        },
        "django_userforeignkey": {"level": "WARNING"},
    },
}

# REST API
# https://www.django-rest-framework.org/api-guide/settings/
REST_FRAMEWORK = {
    "EXCEPTION_HANDLER": "waecm.core.exceptions.error_handler",
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        # OIDC authentication
        "oidc_auth.authentication.JSONWebTokenAuthentication",
        # Basic authentication for the browsable API and Swagger
        "rest_framework.authentication.BasicAuthentication",
        "rest_framework.authentication.SessionAuthentication",
    ),
    "DEFAULT_PAGINATION_CLASS": "rest_framework.pagination.LimitOffsetPagination",
    "PAGE_SIZE": None,
}

# https://github.com/ByteInternet/drf-oidc-auth
OIDC_AUTH = {
    # Specify OpenID Connect endpoint. Configuration will be automatically done based on the
    # discovery document found at <endpoint>/.well-known/openid-configuration
    "OIDC_ENDPOINT": env("OIDC_ENDPOINT", default="https://waecm-sso.inso.tuwien.ac.at/auth/realms/waecm"),
    # https://docs.authlib.org/en/latest/jose/jwt.html#jwt-payload-claims-validation
    "OIDC_CLAIMS_OPTIONS": {},
    "JWT_AUTH_HEADER_PREFIX": env("AUTH_HEADER_PREFIX", default="Bearer"),
    # (Optional) Function that resolves id_token into user.
    # This function receives a request and an id_token dict and expects to return a User object.
    # The default implementation tries to find the user based on username (natural key) taken from
    # the 'sub'-claim of the id_token.
    "OIDC_RESOLVE_USER_FUNCTION": "waecm.core.authentication.resolve_user_from_oidc",
}

# CORS
# https://github.com/ottoyiu/django-cors-headers
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_METHODS = (
    "GET",
    "POST",
    "PUT",
    "PATCH",
    "DELETE",
    "OPTIONS",
)
CORS_EXPOSE_HEADERS = ("Filename", "Content-Type", "Content-Disposition")

SWAGGER_SETTINGS = {
    # use OpenAPI 3.0 schema from DRF instead of OpenAPI 2.0 schema from YASG
    "SPEC_URL": reverse_lazy("openapi-schema"),
}

MAX_FEEDS_PER_USER = env("MAX_FEEDS_PER_USER", default=3)
MAX_KEYWORDS_PER_FEED = env("MAX_KEYWORDS_PER_FEED", default=3)
MIN_KEYWORD_LENGTH = env("MIN_KEYWORD_LENGTH", default=3)
MAX_FEED_ICON_FILE_SIZE_IN_KB = env("MAX_FEED_ICON_FILE_SIZE_IN_KB", default=10)

ALLOWED_FEED_ICON_EXTENSIONS = env.list("ALLOWED_FEED_ICON_EXTENSIONS", default=["jpg", "jpeg", "png", "svg"])

TWITTER_API_KEY = env("TWITTER_API_KEY", default=None)
TWITTER_API_SECRET_KEY = env("TWITTER_API_SECRET_KEY", default=None)
TWITTER_ACCESS_TOKEN = env("TWITTER_ACCESS_TOKEN", default=None)
TWITTER_ACCESS_TOKEN_SECRET = env("TWITTER_ACCESS_TOKEN_SECRET", default=None)

TWITTER_MAX_TWEET_LENGTH = env("TWITTER_MAX_TWEET_LENGTH", default=140)

# https://developer.twitter.com/en/docs/counting-characters
# all URLs are wrapped in t.co links with 23 characters
TWITTER_URL_LENGTH = env("TWITTER_URL_LENGTH", default=23)

TWITTER_ELLIPSIS_SYMBOL = env("TWITTER_ELLIPSIS_SYMBOL", default="…")  # https://en.wikipedia.org/wiki/Ellipsis

BOT_INTERVAL_SECONDS = env("BOT_INTERVAL_SECONDS", default=60)
