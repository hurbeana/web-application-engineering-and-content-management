# WAECM Backend

## Configuration

Credentials and other sensitive information can be configured in the `app/.env` file.

Static configuration happens in `app/waecm/settings.py`.

## Interfaces

* Admin panel: http://localhost:8000/admin
* API (browsable): http://localhost:8000/api
* API documentation: http://localhost:8000/openapi
* OpenAPI schema: http://localhost:8000/openapi/schema

## Commands

Create or migrate the database:

```bash
docker-compose run --rm backend "python manage.py migrate"
```

Create a superuser (to access the admin panel):

```bash
docker-compose run --rm backend "python manage.py createsuperuser"
```

Create new migrations (after changing models):

```bash
docker-compose run --rm backend "python manage.py makemigrations"
```

Load all RSS feed news:

```bash
docker-compose run --rm backend "python manage.py read_feeds"
```

Tweet one news-message:

```bash
docker-compose run --rm backend "python manage.py tweet_news"
```

## Auto-formatter setup

We use isort (https://github.com/pycqa/isort) and black (https://github.com/psf/black) for local auto-formatting. The
pre-commit framework (https://pre-commit.com) provides GIT hooks for these tools, so they are automatically applied
before every commit.

Steps to activate:

* Install the pre-commit framework: `pip install pre-commit` (for alternative installation options
  see https://pre-commit.com/#install)
* Activate the framework (from the root directory of the repository): `pre-commit install`

Hint: You can also run the formatters manually at any time with the following command: `pre-commit run --all-files`
(this runs on all currently staged files).

## PyCharm setup

To make the type referencing in PyCharm work, you need to set up the Python interpreter and project structure.

Set up the interpreter:

1. Go to File --> Settings --> Project: tu-waecm --> Python Interpreter
2. Click the gear icon, then "Add..."
3. Choose "Docker Compose" on the left
4. Choose Server: Docker  
   In case you do not have that option, click the "New..." button and add a Server, choosing the "Unix socket"
5. Select the "configuration file": backend-django/docker-compose.yml

Now define the project structure:

1. Go to File --> Settings --> Project: tu-waecm --> Project structure
2. Mark backend-django/app as "Sources"

Also configure Django support, to enable running and debugging the application:

1. Go to File --> Settings --> Languages & Frameworks --> Django
2. Check "Enable Django Support"
3. Set the Django project root to `backend-django/app`
4. Choose `waecm/settings.py` as "Settings"
