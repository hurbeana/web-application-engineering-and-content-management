# Web Application Engineering & Content Management

For Assignment 3 documentation please refer to [Assignment3.md](Assignment3.md).
# Technology Choices
## Backend
* Python
* Django
* Django REST Framework

Django is a popular, tried and tested framework that provides great support for just about anything.
It is easy to develop anything with it, from small to large applications, with less code than other languages/frameworks require.

## Frontend
Vue.js

## Database
Postgres

## Linters
We use linters/formatters in the backend and in the frontend.
Backend:
* black
* isort

They can be used with the pre-commit framework, or they can be called manually:
```sh
docker-compose run --rm backend bash -c "cd waecm && isort . && black ."
```

Frontend:
* eslint

## Tests
We use jest with vue-test-utils for testing the frontend.

## Containerization
* Docker
* docker-compose

# Running the Application(s)

Before running the application make sure to register as a developer with twitter to get the necessary API keys to be able to post to twitter with the bot. You should have following variables:

- `TWITTER_API_KEY`
- `TWITTER_API_SECRET_KEY`
- `TWITTER_ACCESS_TOKEN`
- `TWITTER_ACCESS_TOKEN_SECRET`

write these to a plain textfile (with name such as `twitter_keys`) and prepend them with `export`:

```bash
export TWITTER_API_KEY=tFxxr93N1x2N6xA9exB4jzxe2
export TWITTER_API_SECRET_KEY=uQ7v43cq2fxKJxai1xCiseCPcYDoBJgXo9tF4xjOEj6Ya3g2wV

export TWITTER_ACCESS_TOKEN=5324593176209797185-7ZxNy43PLZAyetxD2JNdbM1qrhZvc8
export TWITTER_ACCESS_TOKEN_SECRET=IDZPE3Lmbb2qQP2L1NH3IS9NpAswz5QMxXXWhah4oPoCj
```

*(the keys here are not valid and only serve as an example)*

following that, put these keys to your environment using the `source` (for bash or similar):

```bash
source twitter_keys
```

Alternatively you can pass every environment variable with the `-e` docker-compose flag.

Make sure you have this setup before starting the project.

## Running Frontend and Backend (production)

To run the **production build using images** from the docker hub use from the root directory:

```
docker-compose up -d
```

The `-d` flag is optional and just for detaching the terminal from the processes. Use `docker-compose logs --follow` to follow the logs, after you have started in a detached mode. Use `docker-compose down` to shut down both services.

Leave the `-d` option out to keep the log in the current terminal session.

This command starts the **backend on port 8000** and the **frontend on port 5000**

## Running Frontend and Backend (development)

To run **both** of the projects in development mode (with code hot reload) run from the root of the project:

```
docker-compose -f docker-compose.dev.yml up --build -d
```

The `-d` flag is optional and just for detaching the terminal from the processes. Use `docker-compose logs --follow` to follow the logs, after you have started in a detached mode. Use `docker-compose down` to shut down both services.

Each application opens its own needed port for development (backend: 8080, frontend: 8000) and binds the relevant source code directories to volumes, so that code can get reloaded if changed (backend: `./app:/app`, frontend: `.:/app`).

## Running standalone

To run **one** of the projects in development mode (with code hot reload) run from the projects folder:

```
docker-compose up --build -d
```

The `-d` flag is optional and just for detaching the terminal from the processes. Use `docker-compose logs --follow` to follow the logs, after you have started in a detached mode. Use `docker-compose down` to shut down both services.

The configuration (ports, volumes etc.) described in the previous section remain the same.

## Building the images

Switch to each project and run

```
docker build -t IMAGENAME:TAG -f Dockerfile.prod .
```

To build an image which doesn't require volumes and thus can be shared and run standalone.

### Pushing the image

To push the image to docker hub, first login with your account by running
```
docker login
```
and entering your credentials. After that make sure you have your images built you want to push, by running `docker images`. Run
```
docker tag IMAGENAME:TAG USERNAME/IMAGENAME:REPOTAG
```
to tag the image with the repo name, to be able to push it, tagged. Afterwards, push the image to the repo with
```
docker push USERNAME/IMAGENAME:REPOTAG
```
you can find the image on [docker hub](https://hub.docker.com)

our CD images can be found at:
- https://hub.docker.com/repository/docker/hurbeana/waecm-2021-group-17-bsp-3-frontend
- https://hub.docker.com/repository/docker/hurbeana/waecm-2021-group-17-bsp-3-backend

# Additional documentation

Additional documentation can be found the sub-folders:

* [Backend ReadMe](backend-django/README.md)
* [Frontend ReadMe](frontend-vue/README.md) 
