const template = document.createElement("template");

template.innerHTML = `
<style>
.container {
  color: rgb(255, 255, 255);
  background-color: rgb(35, 122, 252);
  padding: 1em 1.8em;
  width: 100%;
  font-family: Helvetica,Calibri,Arial,sans-serif;
  display: grid;
  grid-template-columns: repeat(3, minmax(5em, 1fr) );
  grid-template-rows: minmax(100px, auto);
}
.footer {
  position:fixed;
  left:0px;
  bottom:0px;
}
.footerBTN {
    position:fixed;
    right:0px;
    bottom:0px;
  }
.one {
    grid-column: 1;
    grid-row: 1;
  }
  .two {
    grid-column: 2;
    grid-row: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
  }
  .labelCookie {
    font-weight: bold;
    display: flex;
  }
  .btnWrapper {
    grid-column: 3;
    grid-row: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
.button {
    color: rgb(255, 255, 255);
    background-color: transparent;
    border-color: rgb(255, 255, 255);
    padding: 5px 20px;
    cursor: pointer;
    max-height: 30px;
    max-width: 80px
  }
  @media only screen and (max-width: 800px) {
    .one{
      grid-column : 1/ span 2
    }
    .two {
      grid-column: 1;
      grid-row: 2;
      margin-right: 0px;
      flex-direction: column;
      align-items: flex-start;
    }
    .btnWrapper {
      grid-column: 3;
      grid-row: 2;
      justify-content: center;
      align-items:flex-start;
      flex-direction: column;
    }
  }
  .buttonEdit {
    color: rgb(255, 255, 255);
    background-color: rgb(35, 122, 252);
    border-color: rgb(255, 255, 255);
    padding: 5px 40px;
    margin-right: 55px;
    cursor: pointer;
    float:right;
    max-height: 30px
  }

.inline-button {
    display:flex;
}
</style>
<div>
<div class="footerBTN" id="footerBTN">
<button class="buttonEdit">Edit Cookies</button>
</div>
<div id="root" class="container footer">
  <span class="one"></span>
  <div class="two">
  <span class="labelCookie" > Cookies:</span>
  <div class="inline-button">
  <input type="radio" id="necessary" name="cookie1" value="necessary"
         checked disabled>
  <label for="necessary">Necessary</label>
</div>

<div class="inline-button">
  <input type="radio" id="statistics" name="cookie2" value="statistics">
  <label for="statsistics">Statistics</label>
</div>

<div class="inline-button">
  <input type="radio" id="marketing" name="cookie3" value="marketing">
  <label for="marketing">Marketing</label>
</div>
</div>
<div class="btnWrapper">
  <button class="button">Accept</button>
  </div>

</div>
</div>`;

class CookieBanner extends HTMLElement {
  constructor() {
    super();
    this._applicationName = "";
    this.attachShadow({ mode: "open" });
    this._necessary = true;
    this._marketing = false;
    this._statistics = false;
    this._cookies = ["necessary"];
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  static get observedAttributes() {
    return ["application-name", "policy-link"];
  }

  connectedCallback() {
    const root = this.shadowRoot.getElementById("root");
    const cookiesAccepted = getCookie("cookiesAccepted");
    if (Array.isArray(JSON.parse(cookiesAccepted))) {
      root.style.visibility = "hidden";
    } 
      root.querySelector(".button").addEventListener("click", () => {
        const checkEvent = new CustomEvent("on-accept", {detail:{
            necessary : this._necessary,
            marketing: this._marketing,
            statistics: this._statistics
          }
        });
        this.dispatchEvent(checkEvent);
        root.style.visibility = "hidden";
        setCookie("cookiesAccepted", JSON.stringify(this._cookies), 365);
      });
      this.shadowRoot.getElementById("footerBTN").querySelector(".buttonEdit").addEventListener("click", () => {
          root.style.visibility = "visible"
          setCookie("cookiesAccepted", null, 365);
      })
      const statRB = root.querySelector("#statistics");
      statRB.addEventListener("click", () => {
        this._statistics = this.radioButtonModified(statRB,this._statistics,"statistics")
      });
      const markRB = root.querySelector("#marketing")
      markRB.addEventListener("click", () => {
        this._marketing = this.radioButtonModified(markRB,this._marketing,"marketing")
      });
      this.updateMessage();
    
  }
  get applicationName() {
    return this._applicationName;
  }

  get policyLink() {
    return this._policyLink;
  }

  radioButtonModified(rb, value, name) {
      
    value = !value;
    if (value) this._cookies.push(name);
    if (!value) {
      this._cookies = this._cookies.filter((value) => {
        return value !== name;
      });
      rb.checked = false;
    }
    console.log(this._cookies);
    return value
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue !== newValue) {
      if (name === "application-name") {
        this._applicationName = newValue;
        this.updateMessage();
      }
      if (name === "policy-link") {
        this._policyLink = newValue;
        this.updateMessage();
      }
    }
  }

  set applicationName(value) {
    this._applicationName = value;
    this.setAttribute("application-name", value);
    this.updateMessage();
  }

  set policyLink(value) {
    this._policyLink = value;
    this.setAttribute("policy-link", value);
    this.updateMessage();
  }

  updateMessage() {
    if (this.shadowRoot.querySelector("span")) {
      this.shadowRoot.querySelector("span").innerHTML =
        "Um Ihnen den bestmöglichen Service zu gewährleisten speichert " +
        this._applicationName +
        " personenbezogene Daten. Beachten Sie die <a href='" +
        this._policyLink +
        "'>Datenschutz-Richtlinie</a>.";
    }
  }
}
customElements.define("cookie-banner", CookieBanner);

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
