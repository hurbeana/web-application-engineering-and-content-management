# Assignment 3

## Continuous Integration

For this part we chose to use gitlabs Continuous Integration, since some of us already were somewhat familiar with. Since one of our team members already had a verified gitlab account (needed to use the free CI minutes), we decided to use GitLab.

As for the steps, we did not quite adhere to the structure given in the exercise, since we only need to build the frontend and our Dockerfile already includes the building/bundling part. Thus for building a production ready version of the frontend, it was sufficient for this exercise to just build the image, which ran the bundling of the frontend. Alternatively we could have built the frontend in the CI and then either changed our Dockerfile or added a new one, which simply copies the build folder instead of building it inside the docker container. Additionally our backend doesn't need to be built since it is written using python and django, thus only a image building step is needed here.

- For linting we have the `StyleChecks` step which runs a job for linting the backend (using `black`) and a job for the frontend (using `eslint`).

- For testing we have the `Test` step, which runs tests for the backend using `pytest` and for the frontend using `jest`.

- The last step `Build` just builds the necessary using docker and pushes both of them to `hurbeana`'s dockerhub repo.

We use a different image for each job, since each job has different dependecies (such as python, node or docker with dind (docker in docker)). Our CI also integrates with the gitlab CI system by pushing the frontend test aritfacs for the tests. Further we are using protected variables for docker login so that they are not visible inside the logs.

## Testing

For testing the frontend we decided to use jest in combination with vue-testing utils. Jest is the state of the art for frontend testing, recommended by the Vue.js team and offered to be set up even during the project setup process (but can also be added afterwards with a single command - ``vue add unit-jest``)
This setup allows us to mount the Vue components themselves during tests and provide them with data, props, etc... jest can also export the test results in junit format, which can be integrated with GitLab's CI to show up in the pipeline tests on the project page.

## Web Components

For this part we created a custom web component using JavaScript. The component uses the shadow DOM feature, so that its styling cannot be modified by the environment it is placed in. As specified in the exercise the banner can be modified using the 'application-name' and 'policy-link' attributes. When clicking on 'accept' the 'on-accept' event is triggered, containing a payload consisting of an array with the accepted cookie categories. The selected cookies are also stored in a cookie, so that it is sufficient to select the cookie preferences only once. The component was integrated into the public folder of our application so that it can be served alongside our application after building it. 
