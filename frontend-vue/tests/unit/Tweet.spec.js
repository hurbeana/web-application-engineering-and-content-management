import { shallowMount } from '@vue/test-utils';
import Tweet from '@/components/Tweet.vue';

const factory = (props = {}) => {
  return shallowMount(Tweet, {
    props: props
  });
};

describe('Tweet.vue', () => {
  it('displays the url of the tweet and links to it', () => {
    const testUrl = "https://orf.at/stories/3214714/";
    const wrapper = factory({data: {news: {url: testUrl}}});

    const urlElement = wrapper.find(`a[href="${testUrl}"]`);

    expect(urlElement.exists()).toBeTruthy();
    expect(urlElement.text()).toBe(testUrl);
  });

  it('cuts off displayed urls at 40 characters', () => {
    const testUrl = "a".repeat(41);
    const wrapper = factory({data: {news: {url: testUrl}}});
    const urlElement = wrapper.find(`a[href="${testUrl}"]`);

    expect(urlElement.text()).not.toContain(testUrl);
  });

  it('correctly displays date', () => {
    const now = new Date();
    const wrapper = factory({data: {created_at: now.getTime(), news: {}}} );

    expect(wrapper.html()).toContain(now.toLocaleDateString('de-DE'));
    expect(wrapper.html()).toContain(now.toLocaleTimeString('de-DE', {hour: 'numeric', minute:'2-digit'}));
  });

  it('shows twitter icon if no image url is provided', () => {
    const wrapper = factory();

    expect(wrapper.find('i.pi.pi-twitter').exists()).toBeTruthy();
  });

  it('shows provided icon', () => {
    const imgUrl = 'https://rss.orf.at/favicon.ico'
    const wrapper = factory({data: {news: {feed: {icon: imgUrl}}}});

    expect(wrapper.find('img').attributes('src')).toBe(imgUrl);
  });
});