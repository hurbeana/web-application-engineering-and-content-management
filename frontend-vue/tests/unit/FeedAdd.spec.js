import { shallowMount } from '@vue/test-utils';
import FeedAdd from '@/components/FeedAdd.vue';
/*import Button from 'primevue/button';
import Checkbox from 'primevue/checkbox';
import InputSwitch from 'primevue/inputswitch';
import InputText from 'primevue/inputtext';*/

const factory = (values = {}) => {
  return shallowMount(FeedAdd, {
    data () {
      return {
        ...values
      };
    },
    //global: { components: {Button, Checkbox, InputSwitch, InputText}}
  });
};

describe('FeedAdd.vue', () => {
  it('feed is set active by default', () => {
    const wrapper = factory();

    expect(wrapper.find('#feed-active').find('inputswitch').attributes('modelvalue')).toBeTruthy();
  });

  it('does not allow submit if no keywords are entered', () => {
    const wrapper = factory({ url: 'https://rss.orf.at/news.xml' });

    expect(wrapper.find('button[label="Speichern"]').attributes()).toHaveProperty("disabled");
  });

  it('does not allow submit if more than 3 keywords are entered', () => {
    const wrapper = factory({ tags: 'foo, bar, foobar, baz', url: 'https://rss.orf.at/news.xml' });

    expect(wrapper.find('button[label="Speichern"]').attributes()).toHaveProperty("disabled");
  });

  it('does not allow submit if any keyword is shorter than 3 characters', () => {
    const wrapper = factory({ tags: 'abc, d, efg', url: 'https://rss.orf.at/news.xml' });

    expect(wrapper.find('button[label="Speichern"]').attributes()).toHaveProperty("disabled");
  });

  it('does allow submit if there ware between 1 and 3 keywords with at least 3 characters each', () => {
    const wrapper = factory({ tags: 'bird, plane, superman', url: 'https://rss.orf.at/news.xml' });

    expect(wrapper.find('button[label="Speichern"]').attributes()).not.toHaveProperty("disabled");
  });
});