import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Feeds from '@/views/Feeds.vue'
import Tweets from '@/views/Tweets.vue'
import Settings from '@/views/Settings.vue'
import store from '@/store/index.js'
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/feeds',
    name: 'Feeds',
    component: Feeds,
    children: [
      {
          path: '',
          name: 'Tweets',
          component: Tweets
      },
        {
            path: 'settings',
            name: 'settings',
            component: Settings
        },
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})
router.beforeEach((to, from, next) => {
  const unauthAllowed = ['Home', 'Feeds']
  console.log(to)
  const authorised = store.getters.getToken
  if (!unauthAllowed.includes(to.name) && !authorised && !to.hash) {
    next({ name: 'Home' })
  } else next()
})


export default router
