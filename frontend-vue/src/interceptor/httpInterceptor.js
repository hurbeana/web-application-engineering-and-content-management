import axios from 'axios';
import router from '../router/index'

export default function setup() {
    axios.interceptors.response.use(response => {
        return response;
     }, error => {
       if (error.response.status === 401) {
       router.push('/')
       console.log("New Session")
       }
       return error;
     });
}