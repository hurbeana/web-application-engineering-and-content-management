import { createStore } from 'vuex'
import axios from 'axios'

export default createStore({
  state: {
    oidcConfig: null,
    accessToken:null,
    userData:null,
    idToken:null
  },
  getters: {
    isLoggedIn(state) { return !!state.idToken && state.userData.exp > Date.now()/1000},
    getToken (state) {return state.idToken}
  },
  mutations: {
    updateOidcConfig (state, config) {
      state.oidcConfig = config;
    },
    updateUserData(state,data){
      state.userData=data
    },
    updateAccessToken(state,token){
      state.accessToken=token
    },
    updateIdToken(state,token){
      state.idToken=token
    }

  },
  actions: {
    async getOidcConfig (context) {
      console.log(this.state)
      if (this.state.oidcConfig) return this.state.oidcConfig;

      const oidcConfigUrl = "https://waecm-sso.inso.tuwien.ac.at/auth/realms/waecm/.well-known/openid-configuration";
      const oidcConfig = (await axios.get(oidcConfigUrl)).data
      context.commit('updateOidcConfig', oidcConfig);
      return oidcConfig;
    },


  },
  modules: {
  }
})
