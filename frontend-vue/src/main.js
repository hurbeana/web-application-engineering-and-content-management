import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config';
import router from './router'
import store from './store'
import Button from 'primevue/button';
import Card from 'primevue/card';
import Checkbox from 'primevue/checkbox';
import InputSwitch from 'primevue/inputswitch';
import InputText from 'primevue/inputtext';
import 'primevue/resources/primevue.min.css'
import 'primevue/resources/themes/bootstrap4-light-blue/theme.css'
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css'
import interceptors from './interceptor/httpInterceptor'

interceptors()
const app = createApp(App)
//app.config.isCustomElement = tag => tag.startsWith('cookie')
app.use(store).use(router).use(PrimeVue).component('Button',Button).component('Card',Card).component('InputText',InputText).component('Checkbox',Checkbox).component('InputSwitch',InputSwitch)
    .mount('#app')
